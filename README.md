# Awesome Automators [![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

> A curated list of applications and tips from the [Automators](https://automators.fm/) podcast. 
>
> This list was put together by the AppJawn team - creators of the iOS recipe app [ClipDish](https://getclipdish.com).
>
>Icons provides by [Icons8](https://icons8.com).


## Contents

- [Automation](#automation)
- [Calculators](#calculators)
- [Document Management](#document-management)
- [File Management](#file-management)
- [Journaling](#journaling)
- [Note Taking](#note-taking)
- [Text Editors](#text-editors)
- [Text Expansion](#text-expansion)
- [Window Managers](#window-managers)
- [Miscellaneous](#miscellaneous)

## Automation

Let your device do the work for you.

- [Zapier](https://zapier.com) | web
- [IFTTT](https://ifttt.com/) | web
- [Hazel](https://www.noodlesoft.com/) | ![macOS](icons/icons8-mac-client.png)
- [Bunch](https://bunchapp.co) | ![macOS](icons/icons8-mac-client.png)
- [Hazel](https://www.noodlesoft.com) | ![macOS](icons/icons8-mac-client.png)
- [Launch Center Pro](https://apps.apple.com/us/app/launch-center-pro/id532016360) | ![iOS](icons/icons8-iphone.png)
- [Home Assistant](https://www.home-assistant.io) | web ![macOS](icons/icons8-mac-client.png) ![iOS](icons/icons8-iphone.png)
- [Alfred](https://www.alfredapp.com/) - Swiss army knife app could go in multiple categories. |  ![macOS](icons/icons8-mac-client.png)

## Calculators

- [P-Calc](https://pcalc.com) | ![macOS](icons/icons8-mac-client.png) ![iOS](icons/icons8-iphone.png)


## Document Management

- [Scanner Pro](https://readdle.com/scannerpro)
- Scan Bar
- [PDFPen](https://pdfpen.com) | ![macOS](icons/icons8-mac-client.png) iOS

## File Management

- [ForkLift](https://binarynights.com) | ![macOS](icons/icons8-mac-client.png) | Episode 79
- [Yoink](https://eternalstorms.at/yoink/mac/index.html) | ![macOS](icons/icons8-mac-client.png)

## Journaling

- [Day One](https://dayoneapp.com) ![macOS](icons/icons8-mac-client.png) ![iOS](icons/icons8-iphone.png)

## Text Editors

Need to muck about with text? Look here.

- [BBEdit](https://www.barebones.com/products/bbedit/) ![macOS](icons/icons8-mac-client.png)
- [Drafts](https://getdrafts.com/)
- [Obsidian](https://obsidian.md/) ![macOS](icons/icons8-mac-client.png)
- [Ulysses](https://ulysses.app/) ![macOS](icons/icons8-mac-client.png)

## Text Expansion

Enter a snippet and it expands to whatever you want.

- [TextExpander](https://textexpander.com) | ![macOS](icons/icons8-mac-client.png) ![iOS](icons/icons8-iphone.png) | Episode 79
- [Rocket](https://matthewpalmer.net/rocket/) | ![macOS](icons/icons8-mac-client.png)

## Note Taking

- [GoodNotes](https://www.goodnotes.com) | ![macOS](icons/icons8-mac-client.png)
- [Craft](https://www.craft.do) | ![macOS](icons/icons8-mac-client.png)
- [Roam](https://roamresearch.com) | ![macOS](icons/icons8-mac-client.png)

## Window Managers

- [Moom](https://manytricks.com/moom/) | ![macOS](icons/icons8-mac-client.png)

### Miscellaneous

- [Just Press Record](https://www.openplanetsoftware.com/just-press-record/)
- [Trak.tv](https://trakt.tv) - track what TV and movies you watch 
- Infused | Episode 79
- [Streaks](https://streaksapp.com) | Episode 79
- [Keyboard Maestro](https://www.keyboardmaestro.com/) | ![macOS](icons/icons8-mac-client.png) | Episode 79 
- [Dark Noise](https://darknoise.app) | Episode 79
- Micro.blog | Episode 79
- [Karabiner-Elements](https://pqrs.org/osx/karabiner/) | ![macOS](icons/icons8-mac-client.png) | Episode 79
- [BetterTouchTool](https://folivora.ai)

## Contribute

Contributions welcome but read the [contribution guidelines](contributing.md) first.
